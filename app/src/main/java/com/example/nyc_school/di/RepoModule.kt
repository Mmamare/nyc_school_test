package com.example.nyc_school.di

import com.example.nyc_school.model.api.NycApiRepo
import com.example.nyc_school.model.api.NycApiService
import com.example.nyc_school.model.api.NycRepoImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule{

    @Singleton
    @Provides
    fun provideRepo(nycApiService: NycApiService): NycApiRepo{
        return NycRepoImpl(nycApiService)
    }
}
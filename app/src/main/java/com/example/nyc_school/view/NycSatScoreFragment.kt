package com.example.nyc_school.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.nyc_school.databinding.FragmentNycSatScoreBinding
import com.example.nyc_school.model.data.SatScore
import com.example.nyc_school.model.data.UIState
import com.example.nyc_school.viewModel.NycSchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NycSatScoreFragment : Fragment() {

    private var dbn = ""
    private var _binding: FragmentNycSatScoreBinding? = null
    private val binding:FragmentNycSatScoreBinding
    get() = _binding!!

    private val nycViewModel: NycSchoolViewModel by lazy {
        ViewModelProvider(requireActivity())[NycSchoolViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
          _binding = FragmentNycSatScoreBinding.inflate(layoutInflater)
          dbn = arguments?.getString(DBN).toString()
        nycViewModel.getSatScore(dbn)
        getObserver()
        return binding.root
    }

    private fun getObserver() {
        nycViewModel.satScore.observe(viewLifecycleOwner) { state ->
            when (state) {
                is UIState.Loading -> {
                    binding.apply {
                        tvSatLoadingText.visibility = View.VISIBLE
                    }
                    nycViewModel.getSatScore(dbn)
                }
                is UIState.Error -> {
                    binding.apply {
                        tvSatLoadingText.text = state.errorMessage
                    }
                }
                is UIState.ResponseSchoolSat -> {
                    val satScore = showSchoolByDBN(state.satData)
                    if (satScore==null){
                        binding.apply {
                            tvSatLoadingText.text = "No sat info for school"
                        }

                    }else{
                        binding.apply {
                            tvSatLoadingText.visibility = View.GONE
                            tvSatSchoolName.apply {
                                text = satScore.schoolName
                                visibility = View.VISIBLE
                            }
                            tvAveragesTitle.visibility = View.VISIBLE
                            tvTakers.apply {
                                text = "Takers: ${satScore.testTakers}"
                                visibility = View.VISIBLE
                            }
                            tvAvgReading.apply {
                                text = "Reading: ${satScore.avgReading}"
                                visibility = View.VISIBLE
                            }
                            tvAvgMath.apply {
                                text = "Math: ${satScore.avgMath}"
                                visibility = View.VISIBLE
                            }
                            tvAvgWriting.apply {
                                text =  "Writing: ${satScore.avgWriting}"
                                visibility = View.VISIBLE
                            }
                        }
                    }
                }
                else -> { }
            }
        }

    }
    private fun showSchoolByDBN(satScores: List<SatScore>): SatScore?{
        return  satScores.singleOrNull { it.dbn == dbn }
    }
    companion object {
        private const val DBN = "dbn"
        fun newInstance(dbn: String): NycSatScoreFragment{
            val fragment = NycSatScoreFragment()
            val bundle = Bundle()
            bundle.putString(DBN, dbn)
            fragment.arguments = bundle
            return fragment

        }
    }
}
package com.example.nyc_school.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nyc_school.databinding.NycSchoolItemBinding
import com.example.nyc_school.model.data.NYCSchool

class NycSchoolAdapter(private val school: List<NYCSchool>,
                       private val openSat: (String) ->Unit):
    RecyclerView.Adapter<NycSchoolAdapter.NycSchoolViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= NycSchoolViewHolder (
        NycSchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: NycSchoolViewHolder, position: Int) {
        holder.onBind(school[position])
    }

    override fun getItemCount(): Int {
        return school.size
    }
    inner class NycSchoolViewHolder(private val binding: NycSchoolItemBinding):
    RecyclerView.ViewHolder(binding.root){
        fun onBind(nycSchool: NYCSchool) {
            binding.apply {
                tvSchoolName.text = nycSchool.schoolName
                tvSchoolCityAndZipCode.text = String.format(nycSchool.city
                + "' " + nycSchool.zip)

                btnSatScores.setOnClickListener {
                    openSat(nycSchool.dbn)
                }
            }

        }


    }




}



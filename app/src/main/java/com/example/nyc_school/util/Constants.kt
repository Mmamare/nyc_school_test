package com.example.nyc_school.util

//Https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
//
//https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4

// api data
// Schools: https://data.cityofnewyork.us/resource/s3k6-pzi2.json
// Scores: https://data.cityofnewyork.us/resource/f9bf-2cp4.json
const val BASE_URL = "https://data.cityofnewyork.us/resource/"
const val SCHOOL_END = "s3k6-pzi2.json"
const val SAT_END = "f9bf-2cp4.json"
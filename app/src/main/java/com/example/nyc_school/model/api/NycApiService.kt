package com.example.nyc_school.model.api

import com.example.nyc_school.model.data.NYCSchool
import com.example.nyc_school.model.data.SatScore
import com.example.nyc_school.util.SAT_END
import com.example.nyc_school.util.SCHOOL_END
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NycApiService {

    @GET(SCHOOL_END)
    suspend fun getSchoolInfo(): Response<List<NYCSchool>>

    @GET(SAT_END)
    suspend fun getSatInfo(
        @Query("dbn") dbn: String): Response<List<SatScore>>
}
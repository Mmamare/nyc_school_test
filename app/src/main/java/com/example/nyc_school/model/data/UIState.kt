package com.example.nyc_school.model.data

sealed class UIState {
    data class ResponseListSchool(val schoolData: List<NYCSchool>): UIState()
    data class ResponseSchoolSat(val satData: List<SatScore>): UIState()
    data class Error(val errorMessage: String): UIState()
    data class Loading(val isLoading: Boolean = false): UIState()
}

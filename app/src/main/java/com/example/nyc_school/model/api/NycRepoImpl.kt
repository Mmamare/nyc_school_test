package com.example.nyc_school.model.api

import com.example.nyc_school.model.data.UIState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.conflate
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

interface NycApiRepo{
    suspend fun getSchoolInfo(): Flow<UIState>
    suspend fun getSatInfo(dbn: String): Flow<UIState>
}

class NycRepoImpl @Inject constructor(private val service: NycApiService): NycApiRepo{
    override suspend fun getSchoolInfo(): Flow<UIState> {
        return flow {
            emit(UIState.Loading(true))
            try {
                val data = service.getSchoolInfo()
                if (data.isSuccessful){
                    data.body()?.let {
                        emit(UIState.ResponseListSchool(it))
                    }?: emit(UIState.Error("Empty school"))
                } else{
                    emit(UIState.Error("Failure school response"))
                }

            }catch (e: Exception){
                emit(UIState.Error(e.message!!))
            }
        }.conflate()
    }

    override suspend fun getSatInfo(dbn: String): Flow<UIState> {
        return flow {
            try {
                val data = service.getSatInfo(dbn)
                if (data.isSuccessful){
                    data.body()?.let {
                        emit(UIState.ResponseSchoolSat(it))
                    }?: UIState.Error("Empty Score")
                }else{
                    emit(UIState.Error("Failure SAT response"))
                }
            } catch (e: Exception){
                emit(UIState.Error(e.message!!))
            }
        }.conflate()
    }


}
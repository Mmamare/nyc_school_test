package com.example.nyc_school.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nyc_school.model.api.NycApiRepo
import com.example.nyc_school.model.data.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NycSchoolViewModel @Inject constructor(
    private val repository: NycApiRepo,
    private val dispatcher: CoroutineDispatcher): ViewModel(){

    private val _schoolInfo = MutableLiveData<UIState>()
    val schoolInfo: LiveData<UIState>
    get() = _schoolInfo

    private val _satScore = MutableLiveData<UIState>()
    val satScore: LiveData<UIState>
    get() = _satScore

    init {
        getSchoolInfo()
    }

    private fun getSchoolInfo() {
        CoroutineScope(dispatcher).launch {
            repository.getSchoolInfo()
                .catch {
                    _schoolInfo.postValue(UIState.Error("No school to show"))
                }
                .collect{
                    _schoolInfo.postValue(it)
                }
        }
    }
    fun getSatScore(dbn: String){
        CoroutineScope(dispatcher).launch {
            repository.getSatInfo(dbn)
                .catch {
                    _satScore.postValue(UIState.Error("No scores to show"))
                }
                .collect{
                    _satScore.postValue(it)
                }
        }
    }

    fun setScoreLoading() {
        _satScore.value = UIState.Loading(true)
    }

}